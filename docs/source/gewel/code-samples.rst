.. _code_samples:

Sample Code
===========

Below you will find a number of examples of how you can use
gewel to create simple animations. Each is self-contained
and designed to illustrate just one of two specific concepts.
Each code sample is followed by a gif image that illustrates
the result of running it.

You can combine the concepts illustrated here to create all kinds
of different animations of your own.

Each of the code samples is mostly made up of scripting phase code.
(See :ref:`scripting_phase`.)
We can think of scripting phase code as code that writes a
script describing what various objects in the animation should do
at various times.

The lines in the scripting phase that are most critical to the
feature the code sample is attempting to illustrate are highlighted.

The final two lines of each code sample are rendering phase code.
(See :ref:`rendering_phase`.)
In order to keep the samples simple and portable, and to produce the
animations shown in this document, we simply write each
:py:class:`~gewel.draw.Scene` we produce out to a .gif file.

In many cases, you will want to use a :py:class:`~pynmation.record.Mp4Recorder`
instead of a :py:class:`~pynmation.record.GifRecorder` in the rendering phase.
When you are developing and interactively debugging, you are likely to want to
use a :py:class:`~gewel.player.Player`. When you use a
:py:class:`~gewel.player.Player`, you get interactive controls and you
don't have to wait for the entire scene to render before you start playing it.

If you would like to switch to using a player, replace the rendering phase lines:

.. code-block:: python

    recorder = GifRecorder('solar_system.gif')
    recorder.record(scene)

with

.. code-block:: python

    from gewel.player import Player

    player = Player(scene)
    player.mainloop()

in any of the code samples below. Instead of rendering and saving the scene
to a .gif file, it will pop up an interactive player that looks like this:

.. figure:: ../_static/player.png
    :align: center
    :alt: A player showing a solar-system image.

    A player.

.. note::
    Each of the code samples can be copied by moving to the upper right corner of the
    code sample and clicking on the copy to clipboard icon that appears.

.. contents:: Examples
   :depth: 2
   :local:
   :backlinks: none


Fading
------

.. _fade_to_sample:

Using :py:meth:`~gewel.draw.Drawable.fade_to`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        Here is an example of how :py:meth:`~fade_to` works. We will
        create a :py:class:`MarkerDot`, which is a class derived from
        :py:class:`~gewel.draw.Drawable`
        and give it an initial alpha of 0.0, meaning
        it is entirely transparent. We will then fade it in to full opacity (alpha = 1.0)
        and then fade it back to 0.0 so it is fully transparent again.
        We will then wrap it in a
        :py:class:`~gewel.draw.Scene` and save that scene to a .gif file.

        .. literalinclude:: /samples/fade_to.py
            :language: Python
            :linenos:
            :emphasize-lines: 11-12

        That code produces the following gif as output:

        .. figure:: /samples/fade_to.gif
            :width: 640px
            :align: center
            :height: 480px
            :alt: Animated output of the code above.

            The output resulting from our use of :py:meth:`~gewel.draw.Drawable.fade_to`.

Moving and Rotating
-------------------

.. _move_to_sample:

Using :py:meth:`~gewel.draw.XYDrawable.move_to`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is an example of how :py:meth:`~gewel.draw.XYDrawable.move_to` works. We will
create a :py:class:`~gewel.draw.XYDrawable` and move it, then wrap it in a
:py:class:`~gewel.draw.Scene` and save that scene to a .gif file.

.. literalinclude:: /samples/move_to.py
  :language: Python
  :linenos:
  :emphasize-lines: 11

That code produces the following gif as output:

.. figure:: /samples/move_to.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from our use of :py:meth:`~gewel.draw.XYDrawable.move_to`.


.. _rotate_to_sample:

Using :py:meth:`~gewel.draw.XYDrawable.rotate_to`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is an example of how :py:meth:`~gewel.draw.XYDrawable.rotate_to` works. We will
create a :py:class:`~gewel.draw.XYDrawable` and rotate it, then wrap it in a
:py:class:`~gewel.draw.Scene` and save that scene to a .gif file.

.. literalinclude:: /samples/rotate_to.py
  :language: Python
  :linenos:
  :emphasize-lines: 13

That code produces the following gif as output:

.. figure:: /samples/rotate_to.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from our use of :py:meth:`~gewel.draw.XYDrawable.rotate_to`
    (or :py:meth:`~gewel.draw.XYDrawable.rotate_to` as in
    :ref:`rotate_to_degrees_sample` below).

.. _rotate_to_degrees_sample:

Using :py:meth:`~gewel.draw.XYDrawable.rotate_to_degrees`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In :ref:`rotate_to_sample` we used :py:meth:`~gewel.draw.XYDrawable.rotate_to`
to rotate a :py:class:`~gewel.draw.XYDrawable`. If you prefer
degrees to radians, you can use :py:meth:`~gewel.draw.XYDrawable.rotate_to_degrees`
instead of :py:meth:`~gewel.draw.XYDrawable.rotate_to`. All you have to
do is change line 13 in the code above to line 11 in the code below
and remove the now unneeded ``import numpy as np``
at the top to produce the following code:

.. literalinclude:: /samples/rotate_to_degrees.py
  :language: Python
  :linenos:
  :emphasize-lines: 11

That code produces the exact same gif as the one in :ref:`rotate_to_sample`.

.. _marker_sample:

Markers
--------

In the examples above, we created objects of the classes
:py:class:`~gewel.drawable.MarkerDot` and
:py:class:`~gewel.drawable.MarkerUpArrow`, but we did not
properly introduce the classes. Both are derived from the
abstract base class :py:class:`~gewel.drawable.MarkerDot`,
which has other derived classes such as :py:class:`~gewel.drawable.MarkerX`,
:py:class:`~gewel.drawable.MarkerO`, and :py:class:`~gewel.drawable.MarkerPlus`.
These are all illustrated in the following code sample:

.. literalinclude:: /samples/markers.py
    :language: Python
    :linenos:
    :emphasize-lines: 10-14

That code produces the following gif as output:

.. figure:: /samples/markers.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    Various markers in action.

.. _png_sample:

PNGs
----

Markers are good enough for sample animations that are just
trying to teach someone about APIs, but they are rarely
sufficient for real animations. Often you will want to import
.png files created elsewhere into your animations.
:py:class:`~gewel.drawable.PngDrawable` is the class that
lets you do this.

Here is an example of how to construct a :py:class:`~gewel.draw.PngDrawable`
and rotate it 360 degrees, then wrap it in a
:py:class:`~gewel.dwaw.Scene` and save that scene to a .gif file. Note that if
you want to run this code sample you will also need to download
:download:`donut.png </samples/donut.png>` and put it in the same directory
you run the sample code from.

.. literalinclude:: /samples/png_drawable.py
  :language: Python
  :linenos:
  :emphasize-lines: 10

That code produces the following gif as output:

.. figure:: /samples/png_drawable.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    A spinning donut.

Non-Linear Movement and Scaffolding
-----------------------------------

:py:meth:`~gewel.draw.XYDrawable.move_to` is a great way to create
movement in your animations. But it is limited to linear movement. There are
two more advanced methods we can use:
:py:meth:`~gewel.draw.XYDrawable.quadratic_move_to`, which follows
a quadratic path, and :py:meth:`~gewel.draw.XYDrawable.bezier_move_to`,
which follows a cubic path.

When working with these higher degree polynomial paths, it is sometimes
useful to have a visual guide indicating the desired path. We call these
visual guides *scaffolding*. All of the methods that move objects have
and optional ``scaffolding`` argument whose default value is `False`.
If it is set to true, scaffolding is generated and returned as a
:py:class:`~gewel.draw.Drawable`.

We illustrate both quadratic and cubic paths, along with the use of
scaffolding, below.

.. _quad_move_to_sample:

Using :py:meth:`~gewel.draw.XYDrawable.quadratic_move_to`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is an example of how :py:meth:`~gewel.draw.XYDrawable.quadratic_move_to` works. We will
create a :py:class:`~gewel.draw.XYDrawable` and move it, then wrap it in a
:py:class:`~gewel.draw.Scene` and save that scene to a .gif file. In order
to make it more clear what is happening, we will also use the scaffolding
feature to show where the current position, control point, and final
position are, as well as the quadratic path we expect the object to follow.

.. literalinclude:: /samples/quad_move_to.py
    :language: Python
    :linenos:
    :emphasize-lines: 11

That code produces the following gif as output:

.. figure:: /samples/quad_move_to.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from our use of :py:meth:`~gewel.draw.XYDrawable.quadratic_move_to`.

    The path that the object follows is defined by its initial location
    :math:`(x_0, y_0)`, a control point :math:`(x_1, y_1)`, and it's final
    location :math:`(x_2, y_2)`.  The initial location is the location of
    the object before the method is called. The control point and final
    location are specified as parameters.

Notice that the current and final positions as well as the control point and the
path of motion are rendered as scaffolding.

.. _bezier_move_to_sample:

Using :py:meth:`~bezier_move_to`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is an example of how :py:meth:`~gewel.draw.XYDrawable.bezier_move_to` works. We will
create a :py:class:`~gewel.draw.XYDrawable` and move it, then wrap it in a
:py:class:`~gewel.draw.Scene` and save that scene to a .gif file. In order
to make it more clear what is happening, we will also use the scaffolding
feature to show where the current position, two control points, and final
position are, as well as the path we expect the object to follow.

.. literalinclude:: /samples/bezier_move_to.py
    :language: Python
    :linenos:
    :emphasize-lines: 11-14

That code produces the following gif as output:

.. figure:: /samples/bezier_move_to.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from our use of :py:meth:`~gewel.draw.XYDrawable.bezier_move_to`.

The path that the object follows is defined by its initial location
:math:`(x_0, y_0)`, two control points :math:`(x_1, y_1)` and :math:`(x_2, y_2)`,
and it's final location :math:`(x_3, y_3)`.  The initial location is the location of
the object before the method is called. The control points and final
location are specified as parameters.

Notice that the current and final positions as well as the control points and the
path of motion are rendered as scaffolding.

.. _update_time_samples:

Update Time
-----------

Scripting-time methods like :py:meth:`~gewel.draw.XYDrawable.move_to`,
:py:meth:`~gewel.draw.XYDrawable.rotate_to`, and
:py:meth:`~gewel.draw.Drawable.fade_to` take
an optional parameter ``update_time`` that has a default value of
``True``. If this parameter is ``True`` then the object's notion
of what time it should start the next action (known as the *next-action* time)
is increased by the ``duration`` parameter. Setting ``update_time=False`` causes this
not to happen. This enables the object to take several actions
simultaneously.

For example, suppose we want an object to move, but also rotate at the same time.
We can call :py:meth:`~gewel.draw.XYDrawable.move_to` with
``update_time = False``, then call :py:meth:`~gewel.draw.XYDrawable.rotate_to`.
Since the next-action time was not updated during the move, the rotation starts when
the move started.
In addition to the ``update_time`` parameter, we can use the
:py:meth:`~gewel.draw.XYDrawable.wait` method to update the time but not
create any additional motion.

The following example demonstrates several how we can create a variety of effects
by combining these techniques. Note that we also add a :py:class:`~gewel.draw.TimeClock`
to the scene to keep track of time. This class is normally added for visual
debugging during scene development and removed before final rendering. Watch the clock
and the motion and that should help you reason about what the code is doing and
why rotation and motion happen when they do.

.. literalinclude:: /samples/update_time.py
    :language: Python
    :linenos:
    :emphasize-lines: 18,24,27

That code produces the following gif as output:

.. figure:: /samples/update_time.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from our use of the ``update_time = False`` parameter
    to and :py:meth:`~gewel.draw.XYDrawable.move_to` and the method
    :py:meth:`~gewel.draw.Drawable.wait`.

.. _motion_linking_samples:

Motion Linking
--------------

In the examples above, we used scripting methods such as
:py:meth:`~gewel.draw.XYDrawable.bezier_move_to`, with or
without ``update_time=True``, and
:py:meth:`~gewel.draw.Drawable.wait` to script
animation scenes.

In addition to these methods, there is another approach we can use,
which is called *motion linking*. Motion linking connects one or
more properties of a :py:class:`~gewel.draw.Drawable` to that
of another. It can be used to produce effects like one object following
another.

Motion linking is typically done in two steps. First, we create one or
more :py:class:`~gewel.draw.Drawable` objects and script them as
in the examples above. Second, we create one or more additional
:py:class:`~gewel.draw.Drawable` objects and assign one or more
of their properties (typically things like their `x` and `y` location)
to values from the first set of objects.

The key thing to remember is that the properties we are working with are
time-varying values, meaning that they are not fixed, but rather vary over
time. For example, once we have called the
:py:meth:`~gewel.draw.XYDrawable.bezier_move_to` method on an object,
both it's `x` and `y` coordinates are time-varying. That's what produces
the motion. The value at time `t1` is different than the value at `t0`.

When we do motion linking, we step outside the timeline that
:py:meth:`~gewel.draw.XYDrawable.bezier_move_to`,
:py:meth:`~gewel.draw.Drawable.wait_for`, and all the other
methods we discussed above operate on, and assign a property of one object
to match a property of another at all times. This is why we need to fully
script the first object before we do motion linking.

Note that we can also create time-varying values as expressions of
time-varying values and constants. For example, if ``x`` is a time-varying
value then ``v = 2 * x + 10`` is also. The value of ``v`` at any given
time is twice that of ``x`` at the time plus 10.

.. _motion_linking_tracking_samples:

Tracking With Motion Linking
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here is a simple example where we have one object track the `x` coordinate
of another and the `y` coordinate track an expression of the `y` coordinate
of the other.

.. literalinclude:: /samples/motion_track_x.py
    :language: Python
    :linenos:
    :emphasize-lines: 15-23

That code produces the following gif as output:

.. figure:: /samples/motion_track_x.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from our use of motion tracking.

.. _track_sample:

Motion Linking with :py:meth:`~gewel.draw.XYDrawable.track`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In addition to the tracking methods shown above, there
is a :py:meth:`~gewel.draw.XYDrawable.track` method that can be
used to have one object track another at a given `x` and
`y` offset. Here is an example of how it is used.

.. literalinclude:: /samples/motion_track.py
    :language: Python
    :linenos:
    :emphasize-lines: 18

That code produces the following gif as output:

.. figure:: /samples/motion_track.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The output resulting from :py:meth:`~gewel.draw.XYDrawable.track`.


.. _orbit_sample:

A Motion Linked Solar System
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We can now put together some of the motion linking
approaches we just saw above, along with a new one called
:py:meth:`~gewel.draw.XYDrawable.orbit`, to create an animated
solar system. Here is the code:

.. literalinclude:: /samples/solar_system.py
    :language: Python
    :linenos:

That code produces the following gif as output:

.. figure:: /samples/solar_system.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    An animated solar system.

Notice that :py:meth:`~gewel.draw.XYDrawable.track` causes the
labels to track at a fixed offset from the celestial
bodies, even when they follow complicated paths, as
``moon`` does.


.. _partial_motion_linking_samples:

Partial Motion Linking
^^^^^^^^^^^^^^^^^^^^^^

Sometimes we don't want one object to completely track
another, but instead we want just part of it, like an
endpoint or a control point, to track another.

Here is an example, using a :py:class:`~gewel.draw.BezierDrawable`
with it's points linked to other objects. We will
create some :py:class:`~gewel.draw.MarkerPlus` objects for the end points and some
:py:class:`~gewel.draw.MarkerX` objects for the control points. We will then
animate the end points so they move up and down. Next, we will construct
a :py:class:`~gewel.draw.BezierDrawable` from the points. Finally, we will render this all in a
:py:class:`~gewel.draw.Scene` and save that scene to a .gif file.

.. literalinclude:: /samples/linked_bezier.py
  :language: Python
  :linenos:
  :emphasize-lines: 28-35

That code produces the following gif as output:

.. figure:: /samples/linked_bezier.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

Notice that as the end points move, the shape of the Bezier
curve changes accordingly. This is a simple example, but it
illustrates how properties of objects that are time-varying
can be linked.

Here is an another example, using a :py:class:`~gewel.draw.QuadraticDrawable`
with it's points linked to other objects. We will
create some :py:class:`~gewel.draw.MarkerPlus` objects for the end points and a
:py:class:`~gewel.draw.MarkerX` object for the control point. We will then
animate the control point so it moves up and down. Next, we will construct
a :py:class:`~gewel.draw.QuadraticDrawable` from the points. Finally, we will render this all in a
:py:class:`~gewel.draw.Scene` and save that scene to a .gif file.

.. literalinclude:: /samples/linked_quadratic.py
  :language: Python
  :linenos:
  :emphasize-lines: 18-24

That code produces the following gif as output:

.. figure:: /samples/linked_quadratic.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

Notice that as the control point moves, the shape of the quadratic
curve changes accordingly. This is a simple example, but it
illustrates how properties of objects that are time-varying
can be linked.

.. _advanced_motion_linking:

Advanced Motion Linking
-----------------------

We can use expressions and multiple objects to create a variety
of effects with motion linking. In the next example, we will
use motion linking to create wheels on a wagon that rotate as the
wagon moves.

.. literalinclude:: /samples/motion_track_wheels.py
    :language: Python
    :linenos:
    :emphasize-lines: 30-36

That code produces the following gif as output:

.. figure:: /samples/motion_track_wheels.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    The wagon with linked wheels.

Notice that the wagon moves at twice as fast from left to right
as it does from right to left. Since the angle of the wheels is linked
to position, the wheels rotate twice as fast when the wagon is moving
from left to right as they do when it moves from right to left.
We never had to explicitly specify that behavior for the wheels.
The fact that their angle is linked to the `x` position of the wagon
made it happen.

If you would like to learn more about how the time-varying values
(tvx) that enable motion linking work, see :ref:`tvx_intro`.

.. _teleprompter_sample:

Using a Teleprompter
--------------------

:py:class:`~gewel.draw.Teleprompter` objects are boxes of
scrolling text that can be synchronized
to the actions of other objects on the screen. They are typically
used to synchronize voice-over dialog with other actions in
the animation. A voice-over artist can then read along with them
to record an audio track to go with the animation. They can also
be left in the final animation as subtitles.

.. literalinclude:: /samples/teleprompter.py
    :language: Python
    :linenos:
    :emphasize-lines: 9-15,20-29,33-35,37-41

That code produces the following gif as output:

.. figure:: /samples/teleprompter.gif
    :width: 640px
    :align: center
    :height: 480px
    :alt: Animated output of the code above.

    A scene with a teleprompter.


