from gewel.color import ORANGE
from gewel.draw import MarkerUpArrow, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

drawable = MarkerUpArrow(x=32, y=440, height=64, color=ORANGE, line_width=3)

scaffold = drawable.bezier_move_to(
    x1=320, y1=40, x2=580, y2=100, x3=608, y3=440,
    duration=2.0, scaffold=True
)

scene = Scene([background, drawable, scaffold])

# Rendering phase:

recorder = GifRecorder('bezier_move_to.gif')
recorder.record(scene)
