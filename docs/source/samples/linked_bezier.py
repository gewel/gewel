from gewel.color import BLUE, ORANGE, PURPLE, RED
from gewel.draw import Background, BezierDrawable, Scene, MarkerPlus, MarkerX
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

point0 = MarkerPlus(40, 40, line_width=2, color=RED)
point1 = MarkerX(80, 240, line_width=2, color=ORANGE)
point2 = MarkerX(560, 240, line_width=2, color=PURPLE)
point3 = MarkerPlus(600, 440, line_width=2, color=BLUE)

# Make the endpoints move up and down in
# sequence.

point0.move_to(40, 440, duration=1.5)

point3.wait_for(point0)
point3.move_to(600, 40, duration=1.5)

point0.wait_for(point3)
point0.move_to(40, 40, duration=1.5)

point3.wait_for(point0)
point3.move_to(600, 440, duration=1.5)

# A Bezier based on the points above, some
# of which will be in motion.
bezier = BezierDrawable(
    point0.x, point0.y,
    point1.x, point1.y,
    point2.x, point2.y,
    point3.x, point3.y,
)

scene = Scene([background, point0, point1, point2, point3, bezier])

# Rendering phase:

recorder = GifRecorder('linked_bezier.gif')
recorder.record(scene)
