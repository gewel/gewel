from gewel.color import BLACK, DARK_GRAY, RED
from gewel.draw import Box, MarkerPlus, MarkerO, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

# The wagon.
wagon = Box(x=20, y=100, width=160, height=32, color=DARK_GRAY, fill_color=RED)
wagon.move_to(620 - wagon.width, 100, duration=1.5)
wagon.move_to(20, 100, duration=3.0)

# Wheels move with wagon and rotate as it moves.
wheels = []
tires = []
for ii in range(2):
    wheel = MarkerPlus(
        x=wagon.x + 10 + 140 * ii, y=wagon.y + wagon.height + 4,
        width=32, line_width=2, color=DARK_GRAY, z=1.0
    )

    tire = MarkerO(
        x=wheel.x, y=wheel.y,
        width=wheel.width + 2, line_width=5, color=BLACK, z=2.0
    )
    wheels.append(wheel)
    tires.append(tire)

    # Rotate with linear motion of the wagon. When the
    # wheel rotates theta radians, the x distance the
    # wagon travels is theta * r, where r is
    # the radius of the tire. Since we know x,
    # we can compute theta = x / r.
    radius = (tire.width + tire.line_width) / 2
    wheel.theta = wagon.x / radius

# Assemble the scene.
scene = Scene([background, wagon] + wheels + tires)

# Rendering phase:

recorder = GifRecorder('motion_track_wheels.gif')
recorder.record(scene)
