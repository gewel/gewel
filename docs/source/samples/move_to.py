from gewel.color import ORANGE
from gewel.draw import MarkerUpArrow, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

drawable = MarkerUpArrow(x=32, y=240, height=64, color=ORANGE, line_width=3)

drawable.move_to(608, 240, duration=2.0)

scene = Scene([background, drawable])

# Rendering phase:

recorder = GifRecorder('move_to.gif')
recorder.record(scene)
