from typing import Union


FloatOrTVF = Union[float, 'Tvf', 'PyTvf']
BoolOrTVB = Union[bool, 'Tvb', 'PyTvb']
