import numpy as np

import tvx
from gewel.color import DARK_GRAY, BLUE, Color
from gewel.contrib.chart import TvfChartDrawable
from gewel.draw import Background, Scene
from tests.gewel.testutils import SceneComparisonTest


class TestChartDrawable(SceneComparisonTest):
    def test_chart_drawable(self):
        fill_color = Color(0.95, 0.95, 0.95)
        sinusoid = tvx.utils.sine_wave(frequency=2)

        chart_sinusoid = TvfChartDrawable(
            sinusoid, title="sinusoid",
            x=40, y=100, width=560, height=0,
            min_time=0, max_time=3,
            border_color=DARK_GRAY, border_width=2,
            fill_color=fill_color,
            line_color=BLUE, line_width=3,
            time_ticks=range(4),
            value_ticks=np.linspace(-1, 1, 3),
            steps=200
        )

        chart_sinusoid.ramp_attr_to('height', 280, duration=2.0)

        bg = Background()

        scene = Scene([bg, chart_sinusoid])

        self.assertSceneEquals('test_expanding_sinusoid.mp4', scene)
