import unittest

import gewel._timekeeper as timekeeper


class MockTimekeeper(timekeeper.TimekeeperMixin):
    def __init__(self):
        self._time = 0.0

    @property
    def time(self):
        return self._time

    def set_time(self, time: float):
        self._time = time


class TestTimeKeeper(unittest.TestCase):

    def test_wait(self):
        tk = MockTimekeeper()
        self.assertEqual(0.0, tk.time)

        tk.wait(2.0)
        self.assertEqual(2.0, tk.time)

        tk.wait_until(3.0)
        self.assertEqual(3.0, tk.time)

        # If we are waiting until a time in the past,
        # nothing happens.
        tk.wait_until(2.0)
        self.assertEqual(3.0, tk.time)

        tk.wait_until(10.0)
        self.assertEqual(10.0, tk.time)

        for ii in range(10, 20):
            tk.wait_until(ii)
            self.assertEqual(ii, tk.time)

        for ii in range(10):
            tk.wait(1)
            self.assertEqual(20 + ii, tk.time)

    def test_wait_for(self):
        tk1 = MockTimekeeper()
        tk2 = MockTimekeeper()

        tk1.wait(10)
        tk2.wait_for(tk1)

        self.assertEqual(10, tk1.time)
        self.assertEqual(10, tk2.time)

        tk2.wait_for(tk1)
        tk1.wait_for(tk2)

        self.assertEqual(10, tk1.time)
        self.assertEqual(10, tk2.time)

        tk1.wait_until(20)
        tk1.wait_for(tk2)

        self.assertEqual(20, tk1.time)
        self.assertEqual(10, tk2.time)

        tk2.wait_for(tk1)
        tk1.wait_for(tk2)

        self.assertEqual(20, tk1.time)
        self.assertEqual(20, tk2.time)

        tk2.wait_until(30)
        self.assertEqual(30, tk2.time)

        timekeeper.sync([tk1, tk2])

        self.assertEqual(30, tk1.time)
        self.assertEqual(30, tk2.time)

    def test_wait_many(self):

        tk0 = MockTimekeeper()
        tks = [MockTimekeeper() for _ in range(10)]

        for ii, tk in enumerate(tks):
            tk.wait(ii)

        for ii, tk in enumerate(tks):
            self.assertEqual(ii, tk.time)

        tk0.wait_for(tks)
        self.assertEqual(len(tks) - 1, tk0.time)

        tk0.wait_until(100)
        self.assertEqual(100, tk0.time)

        timekeeper.all_wait_for(tks, tk0)
        for tk in tks:
            self.assertEqual(100, tk.time)

        for ii, tk in enumerate(tks):
            tk.wait(ii)

        for ii, tk in enumerate(tks):
           self.assertEqual(100 + ii, tk.time)

        tks[7].wait_until(200)

        timekeeper.sync(tks)

        for tk in tks:
            self.assertEqual(200, tk.time)
